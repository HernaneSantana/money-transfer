# Revolut - Money Transfer ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

A simple RESTful APi for money transfer

## Challenge
Design ​ ​and ​ ​implement ​ ​a ​ ​RESTful ​ ​API ​ ​(including ​ ​data ​ ​model ​ ​and ​ ​the ​ ​backing ​ ​implementation)​ ​for​ ​money
transfers​ ​between ​ ​accounts.

{. . .}

### Implicit ​ ​requirements:
- ​the ​ ​code ​ ​produced ​ ​by​ ​you ​ ​is​ ​expected ​ ​to ​ ​be ​ ​of ​ ​high ​ ​quality.
- ​there ​ ​are ​ ​no ​ ​detailed ​ ​requirements, ​ ​use ​ ​common ​ ​sense.


### API Resources
| Resource | HTTP Method | Resource endpoint | Return |
| ------ | ------ | ------ | ------ | 
| Transfer | POST | /api/v1/transfers | Created Transfer |
| Transfer | GET  | /api/v1/transfers/{transferId} | The requested transfer |
| Transfer | GET  | /api/v1/transfers/query?reference={reference} | The requested transfer |
| Transfer | GET  | /api/v1/transfers | All transfers |

### Context delimitation (business rules)
- This api is not supposed to update/modify a created transfer
- This api is not supposed to delete a created transfer
- Once a transfer is POSTED, if all validations (technical, function and business validation) passes, then the transfer is PAID, otherwise, it is NOT PAID
- A created transfer can only be requested, never modified

## Use case example
### POST a new transfer - JSON
```
POST: http://localhost:8080/api/v1/transfers
```

```
{  
   "accountFrom":"12345",
   "accountTo":"54321",
   "amount":"100",
   "comment":"test"
}
```
### JSON RETURN example
```
{
    "id": 1,
    "version": 0,
    "reference": 4844539857,
    "sourceAccountNumber": 12345,
    "destinationAccountNumber": 54321,
    "amount": 100,
    "createDate": 1536666066157,
    "paid": true,
    "comment": "test",
    "statusCode": "S120",
    "failureMessage": null,
    "_links": {
        "rel": "self",
        "href": "http://localhost:8080/api/v1/transfers/1"
    }
}
```


### Built With
* [Dropwizard](https://www.dropwizard.io/1.3.5/docs/)
* [Maven](https://maven.apache.org/) 

## Usage
### Run the service (command line run)
#### Command line run
```
java -jar money-transfer-0.0.1.jar server config.yml
```

#### Run in Eclipse
Right click in MoneyTransferApplication class -> Run as -> Run Configurations; In the arguments put: "server config.yml"


### Important!
- "server" and "config.yml" are the arguments
- The config.yml file is at the same level as the pom, it must be present in the same directory as the .jar in order to be recognized (dropwizard standard)

## Tests 
- The tests can be executed with Maven and Junit

### Demonstration
For demonstration purposes, the AccountResource was exposed. So you can create a transfer and check the account, ou can create new accounts, etc...


| Resource | HTTP Method | Resource endpoint | Return |
| ------ | ------ | ------ | ------ | 
| Account | POST | /api/v1/accounts | Created account |
| Account | GET  | /api/v1/accounts/{accountId} | The requested account |
| Account | GET  | /api/v1/accounts/query?accountNumber={accountNumber} | The requested account |
| Account | GET  | /api/v1/accounts | All accounts |
| Account | GET  | /api/v1/accounts/demonstration | Create and return some accounts to use in transfers demonstration |

So, to have some accounts to use, just:
```
GET: http://localhost:8080/api/v1/accounts/demonstration
```

### POST a new Account - JSON
```
POST: http://localhost:8080/api/v1/accounts
```
```
{  
   "accountNumber":"12345",
   "balance":"50",
   "startingDate":"2012-07-21",
   "archived":"false",
   "transactionLockEnabled":"false"
}
```
## The .jar

In the Download section there is a money-transfer-0.0.1.jar file

