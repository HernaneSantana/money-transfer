package com.revolut.bank.moneytransfer.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.core.Transfer;
import com.revolut.bank.moneytransfer.service.TransferService;

import io.dropwizard.testing.junit.ResourceTestRule;

@RunWith(MockitoJUnitRunner.class)
public class TransferResourceTest {    
    private static final TransferService transferService = mock(TransferService.class);
    
    @ClassRule
    public static final ResourceTestRule resourceRule = ResourceTestRule.builder()
            .addResource(new TransferResource(transferService))
            .build();
    
    private Transfer transfer;
    private TransferDTO transferDTO;

    @Before
    public void setUp() {
    	transfer = new Transfer(12345L, 54321L, new BigDecimal(50), true, "test", "S010", null);
    	transferDTO = new TransferDTO();
    	transferDTO.setAccountFrom(12345L);
    	transferDTO.setAccountTo(54321L);
    	transferDTO.setAmount(new BigDecimal(100));
    	transferDTO.setComment("test");
    }

    @After
    public void tearDown() {
        reset(transferService);
    }

    @Test
    public void createTransfer_validJsonInput_OK() throws JsonProcessingException {
        when(transferService.create(any(TransferDTO.class))).thenReturn(transfer);
        final Response response = resourceRule.target("/api/v1/transfers")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(transferDTO, MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);        
    }
    
    @Test
    public void createTransfer_noSourceAccount_status422() throws JsonProcessingException {
    	
    	transferDTO = new TransferDTO();    	
    	transferDTO.setAccountTo(54321L);
    	transferDTO.setAmount(new BigDecimal(100));
    	transferDTO.setComment("test");
    	
    	final Response response = resourceRule.client()
                .target("/api/v1/transfers").request()
                .post(Entity.json(new TransferDTO()));

        assertThat(response.getStatus()).isEqualTo(422);
          
    }
    
    @Test
    public void createTransfer_noDestinationAccount_status422() throws JsonProcessingException {
    	
    	transferDTO = new TransferDTO();    	
    	transferDTO.setAccountFrom(12345L);
    	transferDTO.setAmount(new BigDecimal(100));
    	transferDTO.setComment("test");
    	
    	final Response response = resourceRule.client()
                .target("/api/v1/transfers").request()
                .post(Entity.json(new TransferDTO()));

        assertThat(response.getStatus()).isEqualTo(422);
          
    }
    
    @Test
    public void createTransfer_noAmmount_status422() throws JsonProcessingException {
    	
    	transferDTO = new TransferDTO();
    	transferDTO.setAccountFrom(12345L);
    	transferDTO.setAccountTo(54321L);    	
    	transferDTO.setComment("test");
    	
    	final Response response = resourceRule.client()
                .target("/api/v1/transfers").request()
                .post(Entity.json(new TransferDTO()));

        assertThat(response.getStatus()).isEqualTo(422);
          
    }
    
    @Test
    public void createTransfer_nullJsonInput_status422() throws JsonProcessingException {
    	    	
    	final Response response = resourceRule.client()
                .target("/api/v1/transfers").request()
                .post(Entity.json(null));

        assertThat(response.getStatus()).isEqualTo(422);
          
    }

    @Test
    public void getTransfers() throws Exception {
        final List<Transfer> transfers = Collections.singletonList(transfer);
        when(transferService.findAll()).thenReturn(transfers);

        final List<Transfer> response = resourceRule.target("/api/v1/transfers")
            .request().get(new GenericType<List<Transfer>>() {
            });

        verify(transferService).findAll();
        assertThat(response).containsAll(transfers);
    }
}
