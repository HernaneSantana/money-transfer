package com.revolut.bank.moneytransfer.db;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.revolut.bank.moneytransfer.core.Account;

import io.dropwizard.testing.junit.DAOTestRule;

public class AccountDAOTest {
	
    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
        .addEntityClass(Account.class)
        .build();

    private AccountDAO accountDAO;

    @Before
    public void setUp() throws Exception {
        accountDAO = new AccountDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createAccount() {
        final Account account = daoTestRule.inTransaction(() -> accountDAO.save(new Account(12345L, new BigDecimal("100"), Calendar.getInstance().getTime(), false, false)));
        assertThat(account.getId()).isGreaterThan(0);
    }

}
