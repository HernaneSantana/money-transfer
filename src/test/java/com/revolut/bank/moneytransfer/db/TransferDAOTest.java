package com.revolut.bank.moneytransfer.db;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.revolut.bank.moneytransfer.core.Transfer;

import io.dropwizard.testing.junit.DAOTestRule;

public class TransferDAOTest {
	
    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
        .addEntityClass(Transfer.class)
        .build();

    private TransferDAO transferDAO;

    @Before
    public void setUp() throws Exception {
    	transferDAO = new TransferDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createTransfer_validTransfer_transferWithId() {
        final Transfer transfer = daoTestRule.inTransaction(() -> 
        transferDAO.save(new Transfer(12345L, 54321L, new BigDecimal(50), true, "test", "S010", null)));
        assertThat(transfer.getId()).isNotNull();
    }

}
