package com.revolut.bank.moneytransfer;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.junit.ClassRule;
import org.junit.Test;

import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.core.Transfer;

import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

public class MoneyTransferIntegrationTest {

	private static final String TMP_FILE = createTempFile();
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");
    
    @ClassRule
    public static final DropwizardAppRule<MoneyTransferConfiguration> RULE = new DropwizardAppRule<>(
    		MoneyTransferApplication.class, CONFIG_PATH,
            ConfigOverride.config("database.url", "jdbc:h2:" + TMP_FILE));

    @Test
    public void createTransfer_validTransferInput_transferWithID() throws Exception {
    	TransferDTO transferDTO = new TransferDTO();
    	transferDTO.setAccountFrom(12345L);
    	transferDTO.setAccountTo(54321L);
    	transferDTO.setAmount(new BigDecimal(100));
    	transferDTO.setComment("test");
    	
    	Transfer createdTransfer = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/transfers")
    											.request()
    											.post(Entity.entity(transferDTO, MediaType.APPLICATION_JSON_TYPE))
    											.readEntity(Transfer.class);
    	        
        assertThat(createdTransfer.getId()).isNotNull();        
    }
    
    @Test
    public void createTransfer_invalidSourceAccount_transferNotPaid() throws Exception {
    	TransferDTO transferDTO = new TransferDTO();
    	transferDTO.setAccountFrom(-1L);
    	transferDTO.setAccountTo(54321L);
    	transferDTO.setAmount(new BigDecimal(100));
    	transferDTO.setComment("test");
    	
    	Transfer createdTransfer = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/transfers")
    											.request()
    											.post(Entity.entity(transferDTO, MediaType.APPLICATION_JSON_TYPE))
    											.readEntity(Transfer.class);
    	        
        assertThat(createdTransfer.isPaid()).isFalse();        
    }
    
    private static String createTempFile() {
        try {
            return File.createTempFile("test-example", null).getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
	
}
