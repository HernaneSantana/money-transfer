package com.revolut.bank.moneytransfer.bo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.db.AccountDAO;

@RunWith(MockitoJUnitRunner.class)
public class TransferBOTest {
	
	@Mock
	AccountDAO accountDAO;
	
	@Mock
	TransferDTO transferDTO;
	
	@Before
    public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);		
    }
	
	@Test
	public void validateTransferStart_snvalidSourceAccount_sourceAccountNotExists() {
				
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(null));
		   	
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.SRCNOTEXIST)));
				
	}
	
	@Test
	public void validateTransferStart_invalidDestinationAccount_destinationAccountNotExists() {
				
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(new Account()))
		   .thenReturn(Optional.ofNullable(null));
				
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.DSTNOTEXIST)));
				
	}
	
	@Test
	public void validateTransferStart_archivedSourceAccount_sourceAccountIsArchived() {
		
		Account sourceAccountStub = new Account(12345L, new BigDecimal("0"), Calendar.getInstance().getTime(), true, false);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(sourceAccountStub));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.SRCARCHIVED)));
				
	}
	
	@Test
	public void validateTransferStart_archivedDestinationAccount_destinationAccountIsArchived() {
		
		Account destinationAccountStub = new Account(12345L, new BigDecimal("0"), Calendar.getInstance().getTime(), true, false);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(new Account()))
		   .thenReturn(Optional.ofNullable(destinationAccountStub));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.DSTARCHIVED)));
				
	}
	
	@Test
	public void validateTransferStart_sourceEqualsDestination_sameAccount() {
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong())).thenReturn(Optional.ofNullable(new Account()));
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L);
		when(transferDTO.getAccountTo()).thenReturn(12345L);		   
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.SAMEACCNT)));
				
	}
	
	@Test
	public void validateTransferStart_transactionLockedSourceAccount_sourceAccountIsLockedForTransactions() {
		
		Account sourceAccountStub = new Account(12345L, new BigDecimal("0"), Calendar.getInstance().getTime(), false, true);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
										  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(sourceAccountStub))
		   .thenReturn(Optional.ofNullable(new Account()));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.SRCTXLOCK)));
				
	}
	
	@Test
	public void validateTransferStart_transactionLockedDestinationAccount_destinationAccountIsLockedForTransactions() {
		
		Account destinationAccountStub = new Account(12345L, new BigDecimal("0"), Calendar.getInstance().getTime(), false, true);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(new Account()))
		   .thenReturn(Optional.ofNullable(destinationAccountStub));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.DSTTXLOCK)));
				
	}
	
	@Test
	public void validateTransferStart_requestAmountNotValid_amountNotValid() {
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(new Account()))
		   .thenReturn(Optional.ofNullable(new Account()));
		
		when(transferDTO.getAmount()).thenReturn(new BigDecimal(-1));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.AMNTNOTVALID)));
				
	}
	
	@Test
	public void validateTransferStart_sourceAccountHasNotEnoughMoney_notEnoughMoney() {
		
		Account sourceAccountStub = new Account(12345L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(sourceAccountStub))
		   .thenReturn(Optional.ofNullable(new Account()));
		
		when(transferDTO.getAmount()).thenReturn(new BigDecimal(200));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.SRCNOTENGMON)));
				
	}
	
	@Test
	public void validateTransferStart_amountMoreThenTenThousand_limitExceeded() {
		
		Account sourceAccountStub = new Account(12345L, new BigDecimal(20000), Calendar.getInstance().getTime(), false, false);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(sourceAccountStub))
		   .thenReturn(Optional.ofNullable(new Account()));
		
		when(transferDTO.getAmount()).thenReturn(new BigDecimal(11000));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.LMTEXCED)));
				
	}
	
	@Test
	public void validateTransferStart_validAccounts_validated() {
		
		Account sourceAccountStub = new Account(12345L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
		Account destinationAccountStub = new Account(56785L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
		
		when(transferDTO.getAccountFrom()).thenReturn(12345L)
		  								  .thenReturn(54321L);
		
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(sourceAccountStub))
		   .thenReturn(Optional.ofNullable(destinationAccountStub));
		
		when(transferDTO.getAmount()).thenReturn(new BigDecimal(50));
		
		assertThat(TransferBO.validateTransferStart(transferDTO, accountDAO), is(equalTo(TransferResultCode.VALIDATED)));
				
	}
	
	@Test
	public void genetateTransferReference_call_longWithTenDigits() {
		int length = (int)(Math.log10(TransferBO.genetateTransferReference())+1);
		assertThat(length, is(equalTo(10)));
	}
	
	@Test
	public void debitSourceAccount_transferAmount_balanceLessTransferAmount() {
		
		Account sourceAccountStub = new Account(12345L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
						
		when(transferDTO.getAmount()).thenReturn(new BigDecimal(50));
		
		Account accountAfterDebit = TransferBO.debitSourceAccount(sourceAccountStub, transferDTO);
		
		assertThat(accountAfterDebit.getBalance(), is(equalTo(new BigDecimal(50))));
	
	}
	
	@Test
	public void creditDestinationAccount_transferAmount_balancePlusTransferAmount() {
		
		Account destinationAccountStub = new Account(54321L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
						
		when(transferDTO.getAmount()).thenReturn(new BigDecimal(50));
		
		Account accountAfterCredit = TransferBO.creditSourceAccount(destinationAccountStub, transferDTO);
		
		assertThat(accountAfterCredit.getBalance(), is(equalTo(new BigDecimal(150))));
	
	}
	
}
