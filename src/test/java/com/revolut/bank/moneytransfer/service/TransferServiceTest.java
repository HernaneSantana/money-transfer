package com.revolut.bank.moneytransfer.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.bo.TransferBO;
import com.revolut.bank.moneytransfer.bo.TransferResultCode;
import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.core.Transfer;
import com.revolut.bank.moneytransfer.db.AccountDAO;
import com.revolut.bank.moneytransfer.db.TransferDAO;

import mockit.MockUp;
import mockit.Mock;
import static org.mockito.Mockito.mock;


@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

	AccountDAO accountDAO = mock(AccountDAO.class);
	TransferDAO transferDAO = mock(TransferDAO.class);
	TransferDTO transferDTO = mock(TransferDTO.class);
	
	@Before
    public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
    }
	
	@Test
	public void create_validTransferDTO_newTransfer() {
		
		new MockUp<TransferBO>() {
	        @Mock
	        public TransferResultCode validateTransferStart(TransferDTO temptrd, AccountDAO tempacd) {
	            return TransferResultCode.VALIDATED;
	        }
	    };

	    Account sourceAccountStub = new Account(12345L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
		Account destinationAccountStub = new Account(56785L, new BigDecimal(100), Calendar.getInstance().getTime(), false, false);
				
		when(accountDAO.findByAccountNumber(Mockito.anyLong()))
		   .thenReturn(Optional.ofNullable(sourceAccountStub))
		   .thenReturn(Optional.ofNullable(destinationAccountStub));
		
		transferDTO = new TransferDTO();
    	transferDTO.setAccountFrom(12345L);
    	transferDTO.setAccountTo(54321L);
    	transferDTO.setAmount(new BigDecimal(50));
    	transferDTO.setComment("test");
    			
		TransferService transferService = new TransferService(transferDAO, accountDAO);
		
		when(transferDAO.save(any(Transfer.class))).thenReturn(new Transfer());
		
		Transfer createdTransfer = transferService.create(transferDTO);
		
		assertThat(createdTransfer.getReference()).isNotNull();
				
	}
		
}
