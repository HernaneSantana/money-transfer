package com.revolut.bank.moneytransfer.core;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "account")
@NamedQueries({
    @NamedQuery(name="Account.findAll", query="SELECT a FROM Account a"),
    @NamedQuery(name="Account.findByAccountNumber", query="SELECT a FROM Account a WHERE a.accountNumber = :accountNumber")
})
public class Account extends PersistentEntity {

	private static final long serialVersionUID = 7573505035144920159L;
	
	@NotNull
	@Column(name = "account_number", nullable = false)
    private Long accountNumber;

	@NotNull
	@Digits(integer=10, fraction=2)	
    @Column(name = "balance", precision=10, scale=2, nullable = false)
    private BigDecimal balance;
    
	@NotNull
	@Temporal(TemporalType.DATE)
    @Column(name = "starting_date", nullable = false)
    private Date startingDate;
    
	@NotNull	
    @Column(name = "archived", nullable = false)
    private boolean archived;
    
	@NotNull
    @Column(name = "transaction_lock", nullable = false)
    private boolean transactionLockEnabled;
	
	//##### Linking #####
    @Transient
    @JsonSerialize
	private Map<String, String> links;

	public Account() {
		super();
	}

	public Account(Long accountNumber, BigDecimal balance, Date startingDate, boolean archived,
			boolean transactionLockEnabled) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.startingDate = startingDate;
		this.archived = archived;
		this.transactionLockEnabled = transactionLockEnabled;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public boolean isTransactionLockEnabled() {
		return transactionLockEnabled;
	}

	public void setTransactionLockEnabled(boolean transactionLockEnabled) {
		this.transactionLockEnabled = transactionLockEnabled;
	}

	@JsonProperty("_links")
	public Map<String, String> getLinks() {
		return links;
	}

	public void setLinks(Map<String, String> links) {
		this.links = links;
	}
	
}
