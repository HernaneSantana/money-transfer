package com.revolut.bank.moneytransfer.core;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.revolut.bank.moneytransfer.bo.TransferBO;

@Entity
@Table(name = "transfer")
@NamedQueries({
    @NamedQuery(name="Transfer.findByReference", query="SELECT t FROM Transfer t WHERE t.reference = :reference"),
    @NamedQuery(name="Transfer.findByAccountFrom", query="SELECT t FROM Transfer t WHERE t.sourceAccountNumber = :accountFrom"),
    @NamedQuery(name="Transfer.findByAccountTo", query="SELECT t FROM Transfer t WHERE t.destinationAccountNumber = :accountTo"),
    @NamedQuery(name="Transfer.findAll", query="SELECT t FROM Transfer t")
})
public class Transfer extends PersistentEntity {
	
	private static final long serialVersionUID = 1553178935203030650L;

	@Column(name = "trenafer_reference", nullable = false)
    private Long reference;
    	
    @Column(name = "src_account_number", nullable = false)
    private Long sourceAccountNumber;
    
    @Column(name = "dst_account_number", nullable = false)
    private Long destinationAccountNumber;
    
    @Column(name = "transfer_amount", nullable = false)
    private BigDecimal amount;    
    
    @Column(name = "create_date", nullable = false)
    private Date createDate;
    
    @Column(name = "paid", nullable = false)
    private boolean paid;
    
    @Column(name = "comment")
    private String comment;
        
    @Column(name = "status_code", nullable = false)
    private String statusCode;
    
    @Column(name = "failure_message")
    private String failureMessage;
    
    //##### Linking #####
    @Transient
    @JsonSerialize    
	private Map<String, String> links;
    	
    public Transfer() {    	
		super();
		this.reference = TransferBO.genetateTransferReference();
		this.createDate = Calendar.getInstance().getTime();
	}

	public Transfer(Long sourceAccountNumber, Long destinationAccountNumber, BigDecimal amount, boolean paid,
			String comment, String statusCode, String failureMessage) {
		super();
		this.reference = TransferBO.genetateTransferReference();
		this.createDate = Calendar.getInstance().getTime();
		this.sourceAccountNumber = sourceAccountNumber;
		this.destinationAccountNumber = destinationAccountNumber;
		this.amount = amount;
		this.paid = paid;
		this.comment = comment;
		this.statusCode = statusCode;
		this.failureMessage = failureMessage;
	}

	public Long getReference() {
		return reference;
	}

	public void setReference(Long reference) {
		this.reference = reference;
	}

	public Long getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(Long sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public Long getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(Long destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	@JsonProperty("_links")
	public Map<String, String> getLinks() {
		return links;
	}

	public void setLinks(Map<String, String> links) {
		this.links = links;
	}

}
