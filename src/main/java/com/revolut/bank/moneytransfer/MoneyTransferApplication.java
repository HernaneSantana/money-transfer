package com.revolut.bank.moneytransfer;

import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.core.Transfer;
import com.revolut.bank.moneytransfer.db.AccountDAO;
import com.revolut.bank.moneytransfer.db.TransferDAO;
import com.revolut.bank.moneytransfer.resources.AccountResource;
import com.revolut.bank.moneytransfer.resources.TransferResource;
import com.revolut.bank.moneytransfer.service.AccountService;
import com.revolut.bank.moneytransfer.service.TransferService;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MoneyTransferApplication().run(args);
    }
    
    private final HibernateBundle<MoneyTransferConfiguration> hibernateBundle =
            new HibernateBundle<MoneyTransferConfiguration>(Account.class, Transfer.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(MoneyTransferConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public String getName() {
        return "money-transfer";
    }

    @Override
    public void initialize(final Bootstrap<MoneyTransferConfiguration> bootstrap) {
    	    	
    	bootstrap.addBundle(hibernateBundle);
    	
    }

    @Override
    public void run(final MoneyTransferConfiguration configuration, final Environment environment) {

    	final AccountDAO accountDAO = new AccountDAO(hibernateBundle.getSessionFactory());
    	final TransferDAO transferDAO = new TransferDAO(hibernateBundle.getSessionFactory());
    	final AccountService accountService = new AccountService(accountDAO);
    	final TransferService transferService = new TransferService(transferDAO, accountDAO);
    	
    	environment.jersey().register(new AccountResource(accountService));
    	environment.jersey().register(new TransferResource(transferService));    	
    	    	    	
    }
    
}
