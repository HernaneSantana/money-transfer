package com.revolut.bank.moneytransfer.api;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class TransferDTO {
	
	@NotNull(message="number must be present")
    private Long accountFrom;
    
	@NotNull(message="number must be present")
    private Long accountTo;
    
	@NotNull(message="to transfer must be present")
    private BigDecimal amount;
	
    private String comment;
    
    public TransferDTO() {
		
	}

	public Long getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(Long accountFrom) {
		this.accountFrom = accountFrom;
	}

	public Long getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(Long accountTo) {
		this.accountTo = accountTo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}    
	
}
