package com.revolut.bank.moneytransfer.resources;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.revolut.bank.moneytransfer.bo.CustomHateoas;
import com.revolut.bank.moneytransfer.bo.RestGetType;
import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.service.AccountService;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

@Path("api/v1/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {
	
	@Context
    UriInfo uriInfo;

    private final AccountService accountService;

    public AccountResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @POST
    @UnitOfWork
    public Account createAccount(@NotNull @Valid Account account) {
    	Account createdAccount = accountService.create(account);    	
    	account.setLinks(CustomHateoas.buildLinks(uriInfo, createdAccount.getId()));
        return createdAccount;
    }
    
    @GET
    @Path("/{accountId}")
    @UnitOfWork
    public Account getAccount(@PathParam("accountId") LongParam accountId) {
    	Account account = accountService.findSafely(RestGetType.ID, accountId.get(), null);    	
    	account.setLinks(CustomHateoas.buildLinks(uriInfo, null));    	    	
        return account;
    }
    
    @GET    
    @UnitOfWork
    public List<Account> getAccounts() {
    	
    	List<Account> accounts = accountService.findAll(); 
    	
    	accounts.forEach(account->{
    		account.setLinks(CustomHateoas.buildLinks(uriInfo, account.getId()));    			
    	});
    	
        return accounts;
    }
    
    @GET
    @Path("/query")
    @UnitOfWork
    public Account queryAccount(@QueryParam("accountNumber") @NotNull Long accountNumber) {
    	Account account = accountService.findSafely(RestGetType.SEARCH, null, accountNumber);    	
    	account.setLinks(CustomHateoas.buildLinks(uriInfo, account.getId()));
    	return account;
    }
    
    @GET
    @Path("/demonstration")
    @UnitOfWork
    public List<Account> populateAccounts() {
    	
    	List<Account> accounts = accountService.findAll();
    	
    	if(accounts.isEmpty()) {
    	
	    	accountService.create(new Account(12345L, new BigDecimal("100"), Calendar.getInstance().getTime(), false, false));
	    	accountService.create(new Account(54321L, new BigDecimal("100"), Calendar.getInstance().getTime(), false, false));
	    	accountService.create(new Account(22222L, new BigDecimal("0"), Calendar.getInstance().getTime(), true, false));
	    	accountService.create(new Account(33333L, new BigDecimal("0"), Calendar.getInstance().getTime(), false, true));
	    	accountService.create(new Account(77777L, new BigDecimal("10000"), Calendar.getInstance().getTime(), false, false));
	    	accountService.create(new Account(88888L, new BigDecimal("500"), Calendar.getInstance().getTime(), true, true));
	    	
	    	accounts = accountService.findAll();
    	
    	}
    	
    	accounts.forEach(account->{
    		account.setLinks(CustomHateoas.buildLinks(uriInfo, account.getId()));    			
    	});
    	
    	return accounts;
    	 
    }

}
