package com.revolut.bank.moneytransfer.resources;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.bo.CustomHateoas;
import com.revolut.bank.moneytransfer.bo.RestGetType;
import com.revolut.bank.moneytransfer.core.Transfer;
import com.revolut.bank.moneytransfer.service.TransferService;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

@Path("api/v1/transfers")
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {
	
	@Context
    UriInfo uriInfo;

    private final TransferService transferService;

    public TransferResource(TransferService transferService) {
        this.transferService = transferService;
    }

    @POST
    @UnitOfWork
    public Transfer createTransfer(@NotNull @Valid TransferDTO transferDTO) {   	
		Transfer transfer = transferService.create(transferDTO);
    	transfer.setLinks(CustomHateoas.buildLinks(uriInfo, transfer.getId()));
        return transfer;					        
    }
    
    @GET
    @Path("/{transferId}")
    @UnitOfWork
    public Transfer getTransfer(@PathParam("transferId") LongParam transferId) {
    	Transfer transfer = transferService.findSafely(RestGetType.ID, transferId.get(), null);
    	transfer.setLinks(CustomHateoas.buildLinks(uriInfo, null));    	    	
        return transfer;
    }
    
    @GET    
    @UnitOfWork
    public List<Transfer> getTransfers() {
    	List<Transfer> transfers = transferService.findAll();
    	
    	transfers.forEach(transfer->{
    		transfer.setLinks(CustomHateoas.buildLinks(uriInfo, transfer.getId()));    			
    	});
    	
    	return transfers;
    }
    
    @GET
    @Path("/query")
    @UnitOfWork
    public Transfer queryTransfer(@QueryParam("reference") @NotNull Long reference) {
    	Transfer transfer = transferService.findSafely(RestGetType.SEARCH, null, reference);
    	transfer.setLinks(CustomHateoas.buildLinks(uriInfo, transfer.getId()));
    	return transfer;
    }

}
