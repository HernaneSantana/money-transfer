package com.revolut.bank.moneytransfer.bo;

public enum TransferResultCode {	
	SRCNOTEXIST("E010"),
	DSTNOTEXIST("E020"),
	SRCARCHIVED("E030"),
	DSTARCHIVED("E040"),
	SRCTXLOCK("E050"),
	DSTTXLOCK("E60"),
	AMNTNOTVALID("E070"),
	SRCNOTENGMON("E080"),
	SAMEACCNT("E090"),
	LMTEXCED("E100"),	
	VALIDATED("S110"),
	TRANSFERED("S120");	
	
	private String value;
	
	TransferResultCode(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
