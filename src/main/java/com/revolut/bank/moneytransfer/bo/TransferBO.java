package com.revolut.bank.moneytransfer.bo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.db.AccountDAO;

public class TransferBO {
	
	public static TransferResultCode validateTransferStart(TransferDTO transferDTO, AccountDAO accountDAO) {

		Optional<Account> sourceAccount = accountDAO.findByAccountNumber(transferDTO.getAccountFrom());
		Optional<Account> destinationAccount = accountDAO.findByAccountNumber(transferDTO.getAccountTo());	
		
		if(!sourceAccount.isPresent()) {
			return TransferResultCode.SRCNOTEXIST;
		} else if(!destinationAccount.isPresent()) {
			return TransferResultCode.DSTNOTEXIST;
		} else if(transferDTO.getAccountFrom().equals(transferDTO.getAccountTo())) {
			return TransferResultCode.SAMEACCNT;
		} else if(sourceAccount.get().isArchived()) {
			return TransferResultCode.SRCARCHIVED;
		} else if(destinationAccount.get().isArchived()) {
			return TransferResultCode.DSTARCHIVED;
		} else if(sourceAccount.get().isTransactionLockEnabled()) {
			return TransferResultCode.SRCTXLOCK;
		} else if(destinationAccount.get().isTransactionLockEnabled()) {
			return TransferResultCode.DSTTXLOCK;
		} else if(transferDTO.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			return TransferResultCode.AMNTNOTVALID;
		} else if(!hasEnoughMoney(sourceAccount.get(), transferDTO.getAmount())) {
			return TransferResultCode.SRCNOTENGMON;
		} else if(transferDTO.getAmount().compareTo(new BigDecimal(10000)) == 1) {
			return TransferResultCode.LMTEXCED;
		} else {
			return TransferResultCode.VALIDATED;			
		}
		
	}
		
	public static long genetateTransferReference() {
		return (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
	}
	
	private static boolean hasEnoughMoney(Account sourceAccount, BigDecimal amountToTransfer) {
		
		if(sourceAccount.getBalance().compareTo(amountToTransfer) >= 0) {
			return true;
		}
		
		return false;
		
	}
	
	public static Account debitSourceAccount(Account sourceAccount, TransferDTO transferDTO) {
		sourceAccount.setBalance(sourceAccount.getBalance().subtract(transferDTO.getAmount()));
		return sourceAccount;
	}
	
	public static Account creditSourceAccount(Account destinationAccount, TransferDTO transferDTO) {
		destinationAccount.setBalance(destinationAccount.getBalance().add(transferDTO.getAmount()));
		return destinationAccount;
	}
	
	
	public static Map<TransferResultCode, String> resultFailureMessages() {
		
		Map<TransferResultCode, String> messageMap = new HashMap<>();
		
		messageMap.put(TransferResultCode.SRCNOTEXIST, 	"Source account does not exist");
		messageMap.put(TransferResultCode.DSTNOTEXIST, 	"Destination account does not exist");
		messageMap.put(TransferResultCode.SRCARCHIVED, 	"Source account is archived");
		messageMap.put(TransferResultCode.DSTARCHIVED, 	"Destination account is archived");
		messageMap.put(TransferResultCode.SRCTXLOCK,   	"Source account is locked for new transactions");
		messageMap.put(TransferResultCode.DSTTXLOCK,   	"Destination account is locked for new transactions");
		messageMap.put(TransferResultCode.AMNTNOTVALID, "Transfer amount is not valid");
		messageMap.put(TransferResultCode.SRCNOTENGMON, "Source account has not enough money");
		messageMap.put(TransferResultCode.LMTEXCED, "limit exceeded! The limit is 10000 per transfer");		
		messageMap.put(TransferResultCode.SAMEACCNT, "It is not allowed to transfer to own account");
		
		return messageMap;
		
	}
	
}
