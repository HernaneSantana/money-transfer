package com.revolut.bank.moneytransfer.bo;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriInfo;

public class CustomHateoas {
	
	public static Map<String, String> buildLinks(UriInfo uriInfo, Long id) {
		
		Map<String, String> links = new HashMap<>();
		
		String path = uriInfo.getAbsolutePath().toString();
		
		path = path.replace("/demonstration","").replace("/query", "");
		
		links.put("rel", "self");
		links.put("href", id == null ? path : path + "/" + id);
		
		return links;
		
	}
		
}
