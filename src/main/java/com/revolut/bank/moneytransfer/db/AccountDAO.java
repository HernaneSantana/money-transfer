package com.revolut.bank.moneytransfer.db;

import java.util.List;
import java.util.Optional;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.revolut.bank.moneytransfer.core.Account;

import io.dropwizard.hibernate.AbstractDAO;

public class AccountDAO extends AbstractDAO<Account> {
    public AccountDAO(SessionFactory factory) {
        super(factory);
    }
        
    public Account save(Account account) {
        return persist(account);
    }

    public Optional<Account> findById(Long id) {
        return Optional.ofNullable(get(id));
    }
    
    @SuppressWarnings("unchecked")
	public Optional<Account> findByAccountNumber(Long accountNumber) {
    	Query<Account> query = namedQuery("Account.findByAccountNumber");
    	query.setParameter("accountNumber", accountNumber);
    	return Optional.ofNullable(query.uniqueResult());        
    }

    @SuppressWarnings("unchecked")
    public List<Account> findAll() {
        return list((Query<Account>) namedQuery("Account.findAll"));
    }

}
