package com.revolut.bank.moneytransfer.db;

import java.util.List;
import java.util.Optional;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.core.Transfer;

import io.dropwizard.hibernate.AbstractDAO;

public class TransferDAO extends AbstractDAO<Transfer> {
    public TransferDAO(SessionFactory factory) {
        super(factory);
    }
    
    public Transfer save(Transfer transfer) {
        return persist(transfer);
    }

    public Optional<Transfer> findById(Long id) {
        return Optional.ofNullable(get(id));
    }
        
    @SuppressWarnings("unchecked")
	public Optional<Account> findByAccountNumber(Long accountNumber) {
    	Query<Account> query = namedQuery("Account.findByAccountNumber");
    	query.setParameter("accountNumber", accountNumber);
    	return Optional.ofNullable(query.uniqueResult());        
    }
    
    @SuppressWarnings("unchecked")
	public Optional<Transfer> findByReference(Long reference) {
    	Query<Transfer> query = namedQuery("Transfer.findByReference");
    	query.setParameter("reference", reference);
    	return Optional.ofNullable(query.uniqueResult());    	
    }
    
    @SuppressWarnings("unchecked")
    public List<Transfer> findByAccountFrom(Long accountNumber) {
    	Query<Transfer> query = namedQuery("Transfer.findByAccountFrom");
    	query.setParameter("accountFrom", accountNumber);
        return list((Query<Transfer>) query.getResultList());
    }
    
    @SuppressWarnings("unchecked")
    public List<Transfer> findByAccountTo(Long accountNumber) {
    	Query<Transfer> query = namedQuery("Transfer.findByAccountTo");
    	query.setParameter("accountTo", accountNumber);
        return list((Query<Transfer>) query.getResultList());
    }

    @SuppressWarnings("unchecked")
    public List<Transfer> findAll() {
        return list((Query<Transfer>) namedQuery("Transfer.findAll"));
    }

}
