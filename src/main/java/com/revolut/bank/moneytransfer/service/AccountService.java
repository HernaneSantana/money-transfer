package com.revolut.bank.moneytransfer.service;

import java.util.List;

import javax.ws.rs.NotFoundException;

import com.revolut.bank.moneytransfer.bo.RestGetType;
import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.db.AccountDAO;

public class AccountService {
	
	AccountDAO accountDAO;
	
	public AccountService(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;		
	}
	
	public Account create(Account account) {
		return accountDAO.save(account);
	}
	
	public List<Account> findAll() {
		return accountDAO.findAll();
	}
	
	public Account findSafely(RestGetType restGetType, Long id, Long accountNumber) {
    	
        switch (restGetType) {
            case ID:
            	return accountDAO.findById(id).orElseThrow(() -> new NotFoundException("No such account."));
            case SEARCH:            	
            	return accountDAO.findByAccountNumber(accountNumber).orElseThrow(() -> new NotFoundException("No such account."));
            default:
                throw new IllegalArgumentException("Invalid rest get type: " + restGetType);
        }
    	
    }

}
