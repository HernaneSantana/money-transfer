package com.revolut.bank.moneytransfer.service;

import java.util.List;

import javax.ws.rs.NotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.revolut.bank.moneytransfer.api.TransferDTO;
import com.revolut.bank.moneytransfer.bo.RestGetType;
import com.revolut.bank.moneytransfer.bo.TransferBO;
import com.revolut.bank.moneytransfer.bo.TransferResultCode;
import com.revolut.bank.moneytransfer.core.Account;
import com.revolut.bank.moneytransfer.core.Transfer;
import com.revolut.bank.moneytransfer.db.AccountDAO;
import com.revolut.bank.moneytransfer.db.TransferDAO;

public class TransferService {
		
	private final Logger log = LoggerFactory.getLogger(TransferService.class);
	
	TransferDAO transferDAO;
	AccountDAO accountDAO;
		
	public TransferService(TransferDAO transferDAO, AccountDAO accountDAO) {
		this.transferDAO = transferDAO;
		this.accountDAO = accountDAO;
	}
	
	public Transfer create(TransferDTO transferDTO) {
			
		TransferResultCode transferCode = TransferBO.validateTransferStart(transferDTO, accountDAO);
		
		Transfer transfer = null;
		
		log.info("Starting a new transfer");
		
		if(transferCode.equals(TransferResultCode.VALIDATED)) {			
			transfer = executeTransfer(transferDTO);						
			log.info("Transfer reference: {} : TRANSFERED", transfer.getReference());
		} else {
			transfer = new Transfer(transferDTO.getAccountFrom(), transferDTO.getAccountTo(), transferDTO.getAmount(), false, transferDTO.getComment(), transferCode.getValue(), TransferBO.resultFailureMessages().get(transferCode));
			log.info("Transfer reference: {} : NOT TRANSFERED", transfer.getReference());
		}
			
		return transferDAO.save(transfer);
				
	}
	
	public List<Transfer> findAll() {
		return transferDAO.findAll();
	}
	
	public Transfer findSafely(RestGetType restGetType, Long id, Long reference) {
    	
        switch (restGetType) {
            case ID:
            	return transferDAO.findById(id).orElseThrow(() -> new NotFoundException("No such transfer."));
            case SEARCH:            	
            	return transferDAO.findByReference(reference).orElseThrow(() -> new NotFoundException("No such transfer."));
            default:
                throw new IllegalArgumentException("Invalid rest get type: " + restGetType);
        }
    	
    }
	
	private Transfer executeTransfer(TransferDTO transferDTO) {
		
		Account sourceAccount = accountDAO.findByAccountNumber(transferDTO.getAccountFrom()).get();
		Account destinationAccount = accountDAO.findByAccountNumber(transferDTO.getAccountTo()).get();		
		
		sourceAccount = TransferBO.debitSourceAccount(sourceAccount, transferDTO);
		destinationAccount = TransferBO.creditSourceAccount(destinationAccount, transferDTO);
			
		accountDAO.save(sourceAccount);
		accountDAO.save(destinationAccount);
		
		return new Transfer(transferDTO.getAccountFrom(), transferDTO.getAccountTo(), transferDTO.getAmount(), true, transferDTO.getComment(), TransferResultCode.TRANSFERED.getValue(), null);
		
	}

}
